FROM nginx:1.19-alpine

COPY nginx.conf /etc/nginx/conf.d/default.conf

COPY css/style.css /usr/share/nginx/html/css/style.css
COPY img/photo.jpg /usr/share/nginx/html/img/photo.jpg
COPY index.html /usr/share/nginx/html

CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'